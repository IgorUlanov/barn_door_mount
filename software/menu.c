/*
 * menu.c
 *
 *  Created on: 05 мая 2014 г.
 *      Author: igor
 */

#include "menu.h"
#include "lcd_HD44780.h"
#include "common.h"
#include "keys.h"
#include "step_motor.h"
#include "flash.h"
#include "shot.h"


const PARAM param[]={
		{"рабочий ход     ","дл.1 об. ","ms",&option.speed,5000,65000},  // скорость одного оборота в мс
		{"возврат в начало","дл.1 об. ","ms",&option.fast,5000,65000}, // ускоренный оборот в мс
		{"длительность    ","выдержки ","s ",&option.exposure,1,9999}, // длительность выдержки
		{"длительность    ","паузы    ","s ",&option.pause,0,999}, // длительность паузы
		{"количество      ","кадров   ","  ",&option.shot,1,999}, // количество снимков
		{"контрастность   ","дисплея  ","% ",&option.contrast,0,100}, // контрастность дисплея
};

uint8_t param_change[5];
uint8_t menu_number;
uint8_t param_number;
uint8_t position_change;

void Dec(uint16_t value){
	param_change[0]=value/10000;
	value%=10000;
	param_change[1]=value/1000;
	value%=1000;
	param_change[2]=value/100;
	value%=100;
	param_change[3]=value/10;
	param_change[4]=value%10;
}

uint32_t Hex(void){
	uint32_t value=(uint32_t)param_change[0]*10000;
	value+=(uint16_t)param_change[1]*1000;
	value+=(uint16_t)param_change[2]*100;
	value+=param_change[3]*10;
	value+=param_change[4];
	return value;
}

void menuRefresh(void){
	switch(menu_number){
	case stop:
		lcdStop();
		if (flag.key_new==1){
			flag.key_new=0;
			if (key_l.down==1){
				menu_number=down;
				speedChange(option.fast);
			}
			if (key_l.up==1){
				menu_number=work;
				speedChange(option.speed);
				shotStart();
			}
			if (key_l.set==1){
				param_number=0;
				menu_number=select;
				flag.flash=0;
			}
		}
		break;
	case work:
		shotRefresh();
		lcdWork();
		if (flag.key_new==1){
			flag.key_new=0;
			if ((key_l.down==1)||(key_l.up==1)){
				menu_number=stop;
				speedChange(0);
				shotMake(0);
			}
		}
		break;
	case down:
		lcdDown();
		if (flag.key_new==1){
			flag.key_new=0;
			if ((key_l.down==1)||(key_l.up==1)){
				menu_number=stop;
				speedChange(0);
			}
		}
		break;
	case select:
		lcdSelect(param_number);
		if (flag.key_new==1){
			flag.key_new=0;
			if (key_l.down==1){
				param_number--;
				if (param_number>PARAM_COUNT) param_number=0;
			}
			if (key_l.up==1){
				param_number++;
				if (param_number>PARAM_COUNT) param_number=PARAM_COUNT;
			}
			if (key_l.set==1){
				if (param_number==PARAM_COUNT){
					menu_number=stop;
					if (flag.flash==1){
						flag.flash=0;
						flashSave();
					}
				}
				else{
					lcdChange(param_number);
					menu_number=select_change;
					Dec(*param[param_number].value);
					position_change=4;
				}
			}
		}
		break;
	case select_change:
		if (flag.key_new==1){
			flag.key_new=0;
			if (key_l.down==1) if (position_change!=0) position_change--;
			if (key_l.up==1) if (position_change<6) position_change++;
			if (key_l.set==1){
				if (position_change<5){
					menu_number=change;
				}
				else{
					param_number=0;
					menu_number=select;
				}
			}
		}
		if (position_change<5) cursorBlinked(position_change);
		else cursorOff();
		break;
	case change:
		cursorOn(position_change);
		if (flag.key_new==1){
			flag.key_new=0;
			if (key_l.down==1){
				if (param_change[position_change]>0) param_change[position_change]-=1;
				if (Hex()<param[param_number].min){
					Dec(param[param_number].min);
					for (uint8_t i=0;i<5;i++) symbolOut(i,param_change[i]+0x30);
				}
				else symbolOut(position_change,param_change[position_change]+0x30);
				if (param_number==5) lcdContrast(Hex());
			}
			if (key_l.up==1){
				if (param_change[position_change]<9) param_change[position_change]+=1;
				if (Hex()>param[param_number].max){
					Dec(param[param_number].max);
					for (uint8_t i=0;i<5;i++) symbolOut(i,param_change[i]+0x30);
				}
				else symbolOut(position_change,param_change[position_change]+0x30);
				if (param_number==5) lcdContrast(Hex());
			}
			if (key_l.set==1){
					menu_number=select_change;
					cursorBlinked(position_change);
					if (Hex()!=*param[param_number].value){
						flag.flash=1;
						*param[param_number].value=Hex();
					}
			}
		}
		break;
	}
}

void menuStop(void){
	menu_number=stop;
	speedChange(0);
}
