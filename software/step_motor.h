/*
 * step_motor.h
 *
 *  Created on: 26 апр. 2014 г.
 *      Author: igor
 */

#ifndef STEP_MOTOR_H_
#define STEP_MOTOR_H_

#include <stm32f10x.h>
#define MOTOR_TIMER TIM2
#define STEP_PORT 0x00F0
#define STEP1 0x0010
#define STEP2 0x0020
#define STEP3 0x0040
#define STEP4 0x0080

void stepInit(void);
void speedChange(uint16_t value);

#endif /* STEP_MOTOR_H_ */
