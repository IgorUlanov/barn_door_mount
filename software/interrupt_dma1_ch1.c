/*
 * interrupt_adc.c
 *
 *  Created on: 03 мая 2014 г.
 *      Author: igor
 */
#include "interrupt_dma1_ch1.h"

uint16_t adc_buf[32];
uint16_t adc_result[ADC_COUNT];
uint8_t adc_step;
uint16_t adc;

void DMA1_Channel1_IRQHandler(void){
	if ((DMA1->ISR & DMA_ISR_HTIF1)!=0){
		DMA1->IFCR |= DMA_IFCR_CHTIF1;
		uint16_t add=0;
		for (uint8_t i=0;i<16;i++) add+=adc_buf[i];
		adc_result[adc_step++]=add;
		if (adc_step>ADC_COUNT-1) adc_step=0;
	}
	if ((DMA1->ISR & DMA_ISR_TCIF1)!=0){
		DMA1->IFCR |= DMA_IFCR_CTCIF1;
		uint16_t add=0;
		for (uint8_t i=16;i<32;i++) add+=adc_buf[i];
		adc_result[adc_step++]=add;
		if (adc_step>ADC_COUNT-1) adc_step=0;
	}
}
