/*
 * lcd_HD44780.c
 *
 *  Created on: 26 апр. 2014 г.
 *      Author: igor
 */
#include "lcd_HD44780.h"
#include "flash.h"
#include "interrupt_dma1_ch1.h"
#include "common.h"
#include "shot.h"
#include "menu.h"

//#define Y (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOA_BASE+0x0C-PERIPH_BASE) * 0x20) + (4*6))))
#define RS (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOA_BASE+0x0C-PERIPH_BASE) * 0x20) + (4*7))))
#define RW (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOC_BASE+0x0C-PERIPH_BASE) * 0x20) + (4*4))))
#define E (*((__O unsigned long *) (PERIPH_BB_BASE + ((GPIOC_BASE+0x0C-PERIPH_BASE) * 0x20) + (4*5))))
#define PORT 0x7C07 // 0111 1100 0000 0111
#define AH 1 // Tah = 40ns
#define PW 1 // Tpw = 230ns
#define AS 1 // Tas = 10ns
#define LCD_TIMER TIM3

void delay(uint32_t);

uint8_t display[32];

const uint8_t encode[]={
	//A _ _ _ _ _ _ _ _ Ё _ _ _ _ _ _ _ // ASCII символы
	' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0xA2, ' ', ' ', ' ', ' ', ' ', ' ', ' ', //A ЖКИ коды этих символов
	//B _ _ _ _ _ _ _ _ ё _ _ _ _ _ _ _ // ASCII символы
	' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0xB5, ' ', ' ', ' ', ' ', ' ', ' ', ' ', //B ЖКИ коды этих символов
	//C А Б В Г Д Е Ж З И Й К Л М Н О П // ASCII символы
	'A', 0xA0, 'B', 0xA1, 0xE0, 'E', 0xA3, 0xA4, 0xA5, 0xA6, 'K', 0xA7, 'M', 'H', 'O', 0xA8, //C ЖКИ коды этих символов
	//D Р С Т У Ф Х Ц Ч Ш Щ Ъ Ы Ь Э Ю Я // ASCII символы
	'P', 'C', 'T', 0xA9, 0xAA, 'X', 0xE1, 0xAB, 0xAC, 0xE2, 0xAD, 0xAE, 'b', 0xAF, 0xB0, 0xB1, //D ЖКИ коды этих символов
	//E а б в г д е ж з и й к л м н о п // ASCII символы
	'a', 0xB2, 0xB3, 0xB4, 0xE3, 'e', 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 'o', 0xBE, //E ЖКИ коды этих символов
	//F р с т у ф х ц ч ш щ ъ ы ь э ю я // ASCII символы
	'p', 'c', 0xBF, 'y', 0xE4, 'x', 0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7 //F ЖКИ коды этих символов
};

void port(uint8_t value){
	// вывод данных в порт
	GPIOB->ODR&=~PORT;
	GPIOB->ODR|=(((uint16_t)value & 0x07) | (((uint16_t)value<<7) & 0x7C00));
}

void busy(void){
	uint16_t p;
	RS=0;
	RW=1;
	do{
		delay(AS);
		E=1;
		delay(PW);
		p=(GPIOB->IDR & 0x4000);
		E=0;
	} while (p!=0);
	delay(AH);
	RS=1;
	RW=0;
}

void command(uint8_t value){
	// команда на дисплей
	busy();
	RS=0;
	port(value);
	GPIOB->CRL&=~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2);
	GPIOB->CRL|=(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH&=~(GPIO_CRH_CNF10 | GPIO_CRH_CNF11 | GPIO_CRH_CNF12 | GPIO_CRH_CNF13 | GPIO_CRH_CNF14);
	GPIOB->CRH|=(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	delay(AH);
	E=1;
	delay(PW);
	E=0;
	delay(AS);
	GPIOB->CRL|=(GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1);
	GPIOB->CRL&=~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH|=(GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1 | GPIO_CRH_CNF12_1 | GPIO_CRH_CNF13_1 | GPIO_CRH_CNF14_1);
	GPIOB->CRH&=~(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	RS=1;
}

void command_no_busy(uint8_t value){
	// команда на дисплей
	RS=0;
	port(value);
	GPIOB->CRL&=~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2);
	GPIOB->CRL|=(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH&=~(GPIO_CRH_CNF10 | GPIO_CRH_CNF11 | GPIO_CRH_CNF12 | GPIO_CRH_CNF13 | GPIO_CRH_CNF14);
	GPIOB->CRH|=(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	delay(AH);
	E=1;
	delay(PW);
	E=0;
	delay(AS);
	GPIOB->CRL|=(GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1);
	GPIOB->CRL&=~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH|=(GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1 | GPIO_CRH_CNF12_1 | GPIO_CRH_CNF13_1 | GPIO_CRH_CNF14_1);
	GPIOB->CRH&=~(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	RS=1;
}

void data(uint8_t value){
	// данные на дисплей
	busy();
	port(value);
	GPIOB->CRL&=~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2);
	GPIOB->CRL|=(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH&=~(GPIO_CRH_CNF10 | GPIO_CRH_CNF11 | GPIO_CRH_CNF12 | GPIO_CRH_CNF13 | GPIO_CRH_CNF14);
	GPIOB->CRH|=(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	delay(AH);
	E=1;
	delay(PW);
	E=0;
	delay(AS);
	GPIOB->CRL|=(GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1);
	GPIOB->CRL&=~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRH|=(GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1 | GPIO_CRH_CNF12_1 | GPIO_CRH_CNF13_1 | GPIO_CRH_CNF14_1);
	GPIOB->CRH&=~(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
}

void lcdWrite(uint8_t n, char* adr){
	while(*adr!=0) display[n++]=*adr++;
}

void lcdWork(void){
	lcdWrite(0,"w ");
	lcdDec5(2,option.speed);
	lcdWrite(7,"ms ");
	lcdVoltage(10,adc/10);
	lcdWrite(15,"v");
	lcdDec2(16,shot);
	lcdWrite(18,"к ");
	lcdDec4(20,exposure/10);
	lcdWrite(24,"c  ");
	lcdDec3(26,pause/10);
	lcdWrite(29,"c  ");
	flag.lcd_change=1;
}

void lcdAll(void){
	lcdWrite(7,"ms ");
	lcdVoltage(10,adc/10);
	lcdWrite(15,"v");
	lcdDec2(16,option.shot);
	lcdWrite(18,"к ");
	lcdDec4(20,option.exposure);
	lcdWrite(24,"c  ");
	lcdDec3(26,option.pause);
	lcdWrite(29,"c  ");
	flag.lcd_change=1;
}

void lcdStop(void){
	lcdWrite(0,"s ");
	lcdDec5(2,0);
	lcdAll();
}

void lcdDown(void){
	lcdWrite(0,"d ");
	lcdDec5(2,option.fast);
	lcdAll();
}

void lcdDec2(uint8_t n, uint8_t value){
	value%=100;
	if (value>9) display[n++]=value/10+0x30;
	else display[n++]=' ';
	value%=10;
	display[n++]=value+0x30;
}

void lcdDec3(uint8_t n, uint16_t value){
	uint8_t x=0;
	value%=1000;
	if (value>99){
		display[n++]=value/100+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=100;
	if ((value>9)||(x!=0)){
		display[n++]=value/10+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=10;
	display[n++]=value+0x30;
}

void lcdDec4(uint8_t n, uint16_t value){
	uint8_t x=0;
	value%=10000;
	if (value>999){
		display[n++]=value/1000+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=1000;
	if ((value>99)||(x!=0)){
		display[n++]=value/100+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=100;
	if ((value>9)||(x!=0)) display[n++]=value/10+0x30;
	else display[n++]=' ';
	value%=10;
	display[n++]=value+0x30;
}

void lcdVoltage(uint8_t n, uint16_t value){
	value%=10000;
	if (value>999) display[n++]=value/1000+0x30;
	else display[n++]=' ';
	value%=1000;
	display[n++]=value/100+0x30;
	display[n++]=',';
	value%=100;
	display[n++]=value/10+0x30;
	value%=10;
	display[n++]=value+0x30;
}

void lcdDec5(uint8_t n, uint16_t value){
	uint8_t x=0;
	value%=100000;
	if (value>9999){
		display[n++]=value/10000+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=10000;
	if ((value>999)||(x!=0)){
		display[n++]=value/1000+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=1000;
	if ((value>99)||(x!=0)){
		display[n++]=value/100+0x30;
		x=1;
	}
	else display[n++]=' ';
	value%=100;
	if ((value>9)||(x!=0)) display[n++]=value/10+0x30;
	else display[n++]=' ';
	value%=10;
	display[n++]=value+0x30;
}

void lcdSelect(uint8_t value){
	if (value==PARAM_COUNT){
		lcdWrite(0, "                ");
		lcdWrite(16,"   В Ы Х О Д    ");
	}
	else{
		const char *adr=&param[value].message1[0];
		for (uint8_t i=0;i<16;i++) display[i]=*adr++;
		adr=&param[value].message2[0];
		for (uint8_t i=0;i<9;i++) display[16+i]=*adr++;
		lcdDec5(16+9,*param[value].value);
		adr=&param[value].dim[0];
		for (uint8_t i=0;i<2;i++) display[16+14+i]=*adr++;
	}
	flag.lcd_change=1;
}

void symbol(uint8_t value){
	if (value<160) data(value);
	else data(encode[value-160]);
}

void symbolOut(uint8_t c, uint8_t value){
	command(0x80+0x40+9+c); // установка курсора
	if (value>159) value=encode[value-160];
	data(value);
	command(0x10); // возврат курсора
}

void lcdOut(void){
	// вывод на дисплей из буфера
	command(0x80);
	for (uint8_t n=0;n<16;n++) symbol(display[n]);
	command(0xC0);
	for (uint8_t n=16;n<32;n++) symbol(display[n]);
}

void lcdContrast(uint16_t value){
	LCD_TIMER->CCR1 = 40+(value/2);
}

void lcdChange(uint16_t value){
	lcdWrite(0,"старое   ");
	lcdWrite(16,"новое    ");
	const char *adr=&param[value].dim[0];
	for (uint8_t i=0;i<2;i++) display[14+i]=*adr++;
	adr-=2;
	for (uint8_t i=0;i<2;i++) display[16+14+i]=*adr++;
	lcdDec5(9,*param[value].value);
	lcdDec5(16+9,*param[value].value);
}

void cursorOn(uint16_t value){
	command(0x80+0x40+9+value); // установка курсора
	command(0x0E); // включение курсора
}

void cursorBlinked(uint16_t value){
	command(0x80+0x40+9+value); // установка курсора
	command(0x0F); // включение курсора
}

void cursorOff(void){
	command(0x0C); // выключение курсора
}

void lcdInit(void){
	SysTick_Config(SystemCoreClock / 50);
	RCC->APB2ENR |= RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO;
	GPIOC->CRL &= ~(GPIO_CRL_CNF4 | GPIO_CRL_CNF5);
	GPIOC->CRL |= (GPIO_CRL_MODE4 | GPIO_CRL_MODE5);
	GPIOA->CRL &= ~(GPIO_CRL_CNF6 | GPIO_CRL_CNF7);
	GPIOA->CRL |= (GPIO_CRL_CNF6_1 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7);
	RW=0;
	E=0;
	// порт данных на ввод.
	GPIOB->CRL &= ~(GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1 | GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2);
	GPIOB->CRL |= (GPIO_CRL_CNF0_0 | GPIO_CRL_CNF1_0 | GPIO_CRL_CNF2_0);
	GPIOB->CRH &= ~(GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1 | GPIO_CRH_CNF12_1 | GPIO_CRH_CNF13_1 | GPIO_CRH_CNF14_1);
	GPIOB->CRH &= ~(GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12 | GPIO_CRH_MODE13 | GPIO_CRH_MODE14);
	GPIOB->CRH |= (GPIO_CRH_CNF10_0 | GPIO_CRH_CNF11_0 | GPIO_CRH_CNF12_0 | GPIO_CRH_CNF13_0 | GPIO_CRH_CNF14_0);
	RCC->APB1ENR |= RCC_APB1Periph_TIM3;
	LCD_TIMER->ARR=99; // 20kHz
	LCD_TIMER->PSC=11;
	LCD_TIMER->CCR1=40+(option.contrast/2);
	LCD_TIMER->CCMR1=(TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2);
	LCD_TIMER->CCER=(TIM_CCER_CC1P | TIM_CCER_CC1E);
	LCD_TIMER->CR2=0;
	LCD_TIMER->CR1=TIM_CR1_CEN;
	LCD_TIMER->BDTR |= TIM_BDTR_MOE;
	delay(52500); // 15ms
	command_no_busy(0x30);
	delay(14350); // 4.1ms
	command_no_busy(0x30);
	delay(350); // 100us
	command_no_busy(0x30);
	command(0x38);
	command(0x0C);
	command(0x06);
}
