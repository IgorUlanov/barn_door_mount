/*
 * shot.c
 *
 *  Created on: 08 мая 2014 г.
 *      Author: igor
 */

#include "shot.h"
#include "flash.h"
#include "menu.h"

uint16_t exposure; // длительность выдержки
uint16_t pause; // длительность паузы
uint16_t shot; // количество снимков

void shotInit(void){
	RCC->APB2ENR |= RCC_APB2Periph_GPIOC;
	SHOT->CRH &= ~(GPIO_CRH_CNF10);
	SHOT->CRH |= (GPIO_CRH_MODE10);
	SHOT->BRR = SHOT_PIN;
}

void shotMake(uint8_t value){
	if (value==0) SHOT->BRR = SHOT_PIN;
	else SHOT->BSRR = SHOT_PIN;
}

void shotRefresh(void){
	if (exposure==0){
		shotMake(0);
		if (pause==0){
			shot--;
			if (shot==0){
				// цикл закончен
				menuStop();
			}
			else{
				exposure=option.exposure*10;
				pause=option.pause*10;
				shotMake(1);
			}
		}
		else pause--;
	}
	else exposure--;
}

void shotStart(void){
	exposure=option.exposure*10;
	pause=option.pause*10;
	shot=option.shot;
}
