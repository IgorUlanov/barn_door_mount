/*
 * interrupt_adc.h
 *
 *  Created on: 03 мая 2014 г.
 *      Author: igor
 */

#ifndef INTERRUPT_ADC_H_
#define INTERRUPT_ADC_H_
#include <stm32f10x.h>

#define ADC_COUNT 80
extern uint16_t adc_buf[32];
extern uint16_t adc_result[];
extern uint16_t adc;

#endif /* INTERRUPT_ADC_H_ */
