/*
 * shot.h
 *
 *  Created on: 08 мая 2014 г.
 *      Author: igor
 */

#ifndef SHOT_H_
#define SHOT_H_
#include <stm32f10x.h>

#define SHOT GPIOC
#define SHOT_PIN GPIO_Pin_10

extern uint16_t exposure; // длительность выдержки
extern uint16_t pause; // длительность паузы
extern uint16_t shot; // количество снимков

void shotRefresh(void);
void shotInit(void);
void shotStart(void);
void shotMake(uint8_t value);

#endif /* SHOT_H_ */
