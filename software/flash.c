/*
 * flash.c
 *
 *  Created on: 05 мая 2014 г.
 *      Author: igor
 */

#include "flash.h"

#define FLASH_PAGE 1024
#define SIZE (6*2) // размер структуры в байтах
#define COUNT (FLASH_PAGE/SIZE) // количество структур в странице

OPTION option;
const OPTION flash_option __attribute__ ((section (".options_ld"))) = {
		49732, // скорость одного оборота в мс
		6000, // ускоренный оборот в мс
		55, // длительность выдержки в с
		5, // длительность паузы в с
		99, // количество снимков
		50, // контраст дисплея %
};

const OPTION *flash;

void Search(void){
	flash=&flash_option;
	for (uint_fast8_t i=0;i<COUNT;i++){
		if (flash->speed==0xFFFF) break;
		flash++;
		}
}

void flashLoad(void){
	Search();
	flash--;
	uint16_t *ram=(uint16_t*)&option;
	const uint16_t *fl=(const uint16_t*)flash;
	for (uint8_t i=0;i<SIZE/2;i++) *ram++=*fl++;
}

void flashSave(void){
	Search();
	if ((flash-&flash_option)>=FLASH_PAGE){
		flash=&flash_option;
		FLASH_ErasePage((uint32_t)flash);
		}
	FLASH_Unlock();
	uint16_t *ram=(uint16_t*)&option;
	const uint16_t *fl=(const uint16_t*)flash;
	for (uint_fast8_t i=0;i<SIZE/2;i++){
		FLASH_ProgramHalfWord((uint32_t)fl++,*ram++);
		}
	FLASH_Lock();
}
