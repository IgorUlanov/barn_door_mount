/*
 * keys.h
 *
 *  Created on: 29 апр. 2014 г.
 *      Author: igor
 */

#ifndef KEYS_H_
#define KEYS_H_

#include <stm32f10x.h>

void keysInit(void);
void keysRefresh(void);
typedef union{
	uint8_t byte;
	struct{
		uint8_t up:1;
		uint8_t down:1;
		uint8_t set:1;
	};
}KEY;
extern KEY key_l;

#endif /* KEYS_H_ */
