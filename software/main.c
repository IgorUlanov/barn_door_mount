/*
 * main.cpp
 *
 *  Created on: Sep 28, 2011
 *      Author: igor
 */
#include <stdio.h>

#include <stm32f10x.h>
#include "main.h"
#include "init.h"
#include "keys.h"
#include "lcd_HD44780.h"
#include "step_motor.h"
#include "interrupt_dma1_ch1.h"
#include "menu.h"
#include "flash.h"
#include "shot.h"

#define START ((uint16_t *)0x8000000)

volatile FLAG flag;

volatile uint_fast8_t flags;

int main (void){

Begin_config();
adcConfigure();
Final_config();

flashLoad();
lcdInit();
stepInit();
keysInit();
shotInit();
menuStop();

uint8_t n=0;
while(1){
	if (flag.timer!=0){
		flag.timer=0;
		keysRefresh();
		if (n>4){
			n=0;
			uint32_t add=0;
			for (uint8_t i=0;i<ADC_COUNT;i++) add+=adc_result[i];
			adc=(uint16_t)((float)add/(3.305*ADC_COUNT));
			menuRefresh();
		}
		n++;
	}
	if (flag.lcd_change!=0){
		flag.lcd_change=0;
		lcdOut();
	}
  }
return (0);
}

void delay(uint32_t i){
	// один цикл 1.2 мкс
	volatile uint32_t n=0;
	while (i>n) n++;
}
