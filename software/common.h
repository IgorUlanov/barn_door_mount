/**
 * @file
 *  Created on: 18.07.2012
 *  Author: igor
 */
#ifndef COMMON_H_
#define COMMON_H_
#include <stm32f10x.h>

typedef union{
	uint32_t all;
	struct{
		uint32_t key_change:1; // флаг изменения состояния кнопок
		uint32_t key_new:1; // флаг фиксации нового нажатия
		uint32_t lcd_change:1; // флаг необходимости перерисовать дисплей
		uint32_t ready:1; // флаг
		uint32_t timer:1; // флаг каждые 20ms.
		uint32_t flash:1; // флаг необходимости записи во флеш
	};
}FLAG;
extern volatile FLAG flag;

#endif /* COMMON_H_ */
