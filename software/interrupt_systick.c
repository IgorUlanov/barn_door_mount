/*
 * interrupt_systick.c
 *
 *  Created on: 29 апр. 2014 г.
 *      Author: igor
 */

#include <stm32f10x.h>
#include "main.h"

//Обработчик прерывания системного таймера 50Hz
void SysTick_Handler(void){
	flag.timer=1;
}
