/*
 * lcd_HD44780.h
 *
 *  Created on: 26 апр. 2014 г.
 *      Author: igor
 */

#ifndef LCD_H_
#define LCD_H_
#include <stm32f10x.h>

void lcdInit(void);
void lcdOut(void);	// вывод на дисплей из буфера
void lcdWrite(uint8_t n, char* adr);
void lcdDec2(uint8_t n, uint8_t value);
void lcdDec3(uint8_t n, uint16_t value);
void lcdDec4(uint8_t n, uint16_t value);
void lcdDec5(uint8_t n, uint16_t value);
void lcdVoltage(uint8_t n, uint16_t value);
void lcdWork(void);
void lcdDown(void);
void lcdStop(void);
void lcdSelect(uint8_t value);
void lcdContrast(uint16_t value);
void lcdChange(uint16_t value);
void cursorOn(uint16_t);
void cursorOff(void);
void symbolOut(uint8_t c, uint8_t value);
void cursorBlinked(uint16_t);

#endif /* LCD_H_ */

