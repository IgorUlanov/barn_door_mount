/*
 * menu.h
 *
 *  Created on: 05 мая 2014 г.
 *      Author: igor
 */

#ifndef MENU_H_
#define MENU_H_

#include <stm32f10x.h>

#define PARAM_COUNT 6

enum{stop,work,down,select,select_change,change};

typedef struct{
	char message1[16]; // название параметра
	char message2[9]; // название параметра
	char dim[2]; // размерность парамeтра
	uint16_t *value; // адрес переметра
	uint16_t min; // нижний предел значения
	uint16_t max; // верхний предел значения
}PARAM;

extern const PARAM param[];
extern uint8_t menu_number;

void menuRefresh(void);
void menuStop(void);

#endif /* MENU_H_ */
